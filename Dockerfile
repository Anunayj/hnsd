FROM alpine:latest AS build
RUN apk add --no-cache unbound-dev build-base bash libtool m4 automake autoconf
WORKDIR /build
COPY . /build
RUN ./autogen.sh && ./configure && make

FROM alpine:latest AS runtime
RUN apk add --no-cache unbound
COPY --from=build /build/hnsd /bin/
EXPOSE 53
ENTRYPOINT ["hnsd"]
CMD [ "-r", "0.0.0.0:53" ]
